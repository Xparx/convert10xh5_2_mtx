from setuptools import setup, find_packages

DISTNAME = 'convert10x2mtx'
VERSION = '0.01'
DESCRIPTION = "Helper scripts for converting h5 files produced from 10X genomics to plain matrix.mtx files"
MAINTAINER = 'Andreas Tjärnberg'
MAINTAINER_EMAIL = 'andreas.tjarnberg@nyu.edu'
URL = ''
DOWNLOAD_URL = 'https://pypi.org/project/convert10x2mtx/#files'
LICENSE = 'LGPL'

# with open('README.rst') as f:
#     LONG_DESCRIPTION = f.read()


setup(name=DISTNAME,
      version=VERSION,
      description=DESCRIPTION,
      url=URL,
      author=MAINTAINER,
      author_email=MAINTAINER_EMAIL,
      license=LICENSE,
      packages=find_packages(),
      python_requires='>=3',
      install_requires=[
          'pandas',
          'tables',
          'scipy',
          'click',
      ],
      entry_points={'console_scripts': ["h5genomes=convert10x2mtx:print_genomes",
                                        "h5shape=convert10x2mtx:data_shape",
                                        "h5convert2mtx=convert10x2mtx:convert_files"]},
      zip_safe=False)
