import pandas as pd
import os
import scipy.sparse as sp_sparse
from scipy import io
import tables
import click


def get_shape_from_h5(filename, genome):
    """
    Simple function for getting the shape of dataset associated
    with the genome in file. How many genes and how many cells are
    measured."""

    if genome is None:

        genome = get_genome_from_h5(filename)[0]

    with tables.open_file(filename, 'r') as f:
        try:
            group = f.get_node(f.root, genome)
        except tables.NoSuchNodeError:
            print("That genome does not exist in this file.")
            return None

        shape = getattr(group, 'shape').read()

    return shape, genome


def get_genome_from_h5(filename):
    """
    List the genomes availible in the specified file.
    """

    with tables.open_file(filename, 'r') as f:
        try:
            genome = list(f.root._v_children.keys())

        except:
            print("Something went wrong finding a genome")
            return None

    return genome


# inspired by https://support.10xgenomics.com/single-cell-gene-expression/software/pipelines/latest/advanced/h5_matrices
def get_matrix_from_h5(filename, genome):

    with tables.open_file(filename, 'r') as f:
        try:
            group = f.get_node(f.root, genome)
        except tables.NoSuchNodeError:
            print("That genome does not exist in this file.")
            return None
        gene_ids = getattr(group, 'genes').read()
        gene_names = getattr(group, 'gene_names').read()
        barcodes = getattr(group, 'barcodes').read()
        data = getattr(group, 'data').read()
        indices = getattr(group, 'indices').read()
        indptr = getattr(group, 'indptr').read()
        shape = getattr(group, 'shape').read()

    matrix = sp_sparse.csc_matrix((data, indices, indptr), shape=shape)

    data = {'genes': gene_ids, 'gene_names': gene_names, 'barcodes': barcodes, 'matrix': matrix}
    return data


def write_matrix_to_dir(data, outdir, genome):

    full_path = os.path.join(outdir, genome)
    if not os.path.isdir(full_path):
        os.makedirs(full_path, exist_ok=True)

    genes = pd.DataFrame()
    genes['gene_names'] = data['gene_names'].astype(str)
    genes['gene_ids'] = data['genes'].astype(str)
    genes.to_csv(os.path.join(outdir, genome, 'genes.tsv'), header=False, sep='\t', index=False)

    barcodes = pd.DataFrame()
    barcodes['barcodes'] = data['barcodes']
    barcodes.to_csv(os.path.join(outdir, genome, 'barcodes.tsv'), header=False, sep='\t', index=False)

    io.mmwrite(os.path.join(outdir, genome, 'matrix.mtx'), data['matrix'])


def convert(infiles, outdir=None, genome=None):

    for f in infiles:
        if genome is None:
            g = get_genome_from_h5(f)[0]
        else:
            g = genome

        data = get_matrix_from_h5(f, g)

        if outdir is None:
            flist = list(os.path.split(f))
            flist[-1] = flist[-1].split('.')[0]
            outd = os.path.join(*flist)
        else:
            outd = outdir

        write_matrix_to_dir(data, outd, g)


@click.command()
@click.argument("filename", type=click.Path(exists=True))
@click.option("-g", "--genome", nargs=1, default=None, type=str, help='Name of genome to get sample and gene size of.')
def data_shape(filename, genome):

    shape, genome = get_shape_from_h5(filename, genome)
    print('genome={},\tgenes={},\tcells={}'.format(genome, shape[0], shape[1]))


@click.command()
@click.argument("filename", type=click.Path(exists=True))
def print_genomes(filename):

    genomes = get_genome_from_h5(filename)

    print(", ".join(genomes))


@click.command()
@click.argument("infiles", nargs=-1, type=click.Path(exists=True))
@click.option("-o", "--outdir", nargs=1, default=None, type=click.Path(), help="Path to output directory. Will be created if it does not exist. Defaults to input filename")
@click.option("-g", "--genome", nargs=1, default=None, type=str, help="What genome in the 10x file to convert. Default to the first one")
def convert_files(infiles, outdir, genome):

    # if genome == "":
    #     genome = None

    convert(infiles, outdir, genome)


def main(infiles, outdir=None, genome=None):

    convert(infiles, outdir=outdir, genome=genome)


if __name__ == '__main__':
    main()
