# Simple conversion from h5 to mtx format

Simple convert script from 10X h5 format to matrix mtx format ([source](https://support.10xgenomics.com/single-cell-gene-expression/software/pipelines/latest/advanced/h5_matrices)).

## Install

    git clone https://gitlab.com/Xparx/convert10xh5_2_mtx.git
    cd convert10xh5_2_mtx
    pip install --user .

usually this means that the package will be installed in the `~/.local` directory. 
You may have to add the `PATH` variable to `~/.local/bin` to be able to run the scripts. Or the equivalent directory for your OS. 

## Usage

The package give access to 3 scripts, `h5genomes` to list all genomes in the h5 file,
`h5shape` to get the shape of specified genome and `h5convert2mtx` to convert h5 to matrix format.

please use the `--help` flag for specific usage help on each command.

